﻿---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: jaeger-query
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: query-deployment
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: jaeger
        jaeger-infra: query-pod
    spec:
      containers:
      - image: jaegertracing/jaeger-query:1.17.1
        name: jaeger-query
        args: ["--config-file=/conf/query.yaml"]
        ports:
        - containerPort: 16686
          protocol: TCP
        readinessProbe:
          httpGet:
            path: "/"
            port: 16687
        volumeMounts:
        - name: jaeger-configuration-volume
          mountPath: /conf
        env:
        - name: SPAN_STORAGE_TYPE
          valueFrom:
            configMapKeyRef:
              name: jaeger-configuration
              key: span-storage-type
        resources:
          requests:
            memory: 100M
            cpu: 100m
          limits:
            memory: 100M
            cpu: 100m
      volumes:
        - configMap:
            name: jaeger-configuration
            items:
              - key: query
                path: query.yaml
          name: jaeger-configuration-volume
---
apiVersion: v1
kind: Service
metadata:
  name: jaeger-query
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: query-service
spec:
  ports:
  - name: jaeger-query
    port: 16686
    targetPort: 16686
  selector:
    jaeger-infra: query-pod
  type: ClusterIP
---
apiVersion: apps/v1
kind: Ingress
metadata:
 name: jaeger-ui
 namespace: jaeger
 annotations:
   kubernetes.io/ingress.class: nginx
spec:
 rules:
 - host: jaeger.xxx.xxx
   http:
     paths:
     - backend:
         serviceName: jaeger-query
         servicePort: 16686 
---
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: jaeger-collector
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: collector-deployment
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: jaeger
        jaeger-infra: collector-pod
    spec:
      containers:
      - image: jaegertracing/jaeger-collector:1.17.1
        name: jaeger-collector
        args: ["--config-file=/conf/collector.yaml"]
        ports:
        - containerPort: 14267
          protocol: TCP
        - containerPort: 14268
          protocol: TCP
        - containerPort: 9411
          protocol: TCP
        - containerPort: 14250
          protocol: TCP
        readinessProbe:
          httpGet:
            path: "/"
            port: 14269
        volumeMounts:
        - name: jaeger-configuration-volume
          mountPath: /conf
        env:
        - name: SPAN_STORAGE_TYPE
          valueFrom:
            configMapKeyRef:
              name: jaeger-configuration
              key: span-storage-type
      volumes:
        - configMap:
            name: jaeger-configuration
            items:
              - key: collector
                path: collector.yaml
          name: jaeger-configuration-volume
      resources:
        requests:
          memory: 300M
          cpu: 250m
        limits:
          memory: 300M
          cpu: 250m
---
apiVersion: v1
kind: Service
metadata:
  name: jaeger-collector
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: collector-service
spec:
  ports:
  - name: jaeger-collector-tchannel
    port: 14267
    protocol: TCP
    targetPort: 14267
   - name: jaeger-collector-rpc
      port: 14250
      protocol: TCP
      targetPort: 14250
  selector:
    jaeger-infra: collector-pod
  type: ClusterIP
---
---
apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: jaeger-agent
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: agent-daemonset
spec:
  template:
    metadata:
      labels:
        app: jaeger
        jaeger-infra: agent-instance
    spec:
      containers:
      - name: agent-instance
        image: jaegertracing/jaeger-agent:1.17.1
        command:
          - "/go/bin/agent-linux"
          - "--reporter.grpc.host-port=jaeger-collector:14250"
          - "--processor.jaeger-binary.server-queue-size=2000"
          - "--discovery.conn-check-timeout=500ms"
        ports:
        - containerPort: 5775
          protocol: UDP
        - containerPort: 6831
          protocol: UDP
          hostPort: 6831
        - containerPort: 6832
          protocol: UDP
        - containerPort: 5778
          protocol: TCP
        resources:
          requests:
            memory: 200M
            cpu: 200m
          limits:
            memory: 200M
            cpu: 200m
---
apiVersion: v1
kind: Service
metadata:
  name: agent-svc
  namespace: jaeger
  labels:
    app: jaeger
    jaeger-infra: agent-instance
spec:
  ports:
  - name: thrift-compact
    port: 6831
    protocol: UDP
    targetPort: 6831
  - name: thrift-binary
    port: 6832
    protocol: UDP
    targetPort: 6832
  - name: http
    port: 5778
    protocol: TCP
    targetPort: 5778
  - name: zipkin-thrift
    port: 5775
    protocol: UDP
    targetPort: 5775
  - name: health
    port: 14271
    protocol: TCP
    targetPort: 14271
  selector:
    jaeger-infra: agent-instance
  type: ClusterIP
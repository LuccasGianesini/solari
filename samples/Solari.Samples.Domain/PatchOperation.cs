﻿namespace Solari.Samples.Domain
{
    public enum PatchOperation
    {
        Add,
        Remove,
        Update
    }
}
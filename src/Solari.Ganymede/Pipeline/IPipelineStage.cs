﻿namespace Solari.Ganymede.Pipeline
{
    public interface IPipelineStage
    {
        PipelineDescriptor PipelineDescriptor { get; }
    }
}
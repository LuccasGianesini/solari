﻿namespace Solari.Ganymede.Domain
{
    public static class GanymedeConstants
    {
        public const string Basic = "Basic";
        public const string BearerAuth = "Bearer";
        public const string HttpCircuitBraker = "HttpCircuitBraker";
        public const string HttpPolicies = "HttpPolicies";
        public const string HttpRetry = "HttpRetry";
        public const string YamlFileName = "HttpRequestsDefinition";
    }
}
﻿namespace Solari.Deimos.Abstractions
{
    public class DeimosKubernetesOptions
    {
        public string KubernetesService { get; set; }
    }
}
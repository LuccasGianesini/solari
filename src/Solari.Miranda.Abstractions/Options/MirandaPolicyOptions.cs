﻿using System.Collections.Generic;

namespace Solari.Miranda.Abstractions.Options
{
    public class MirandaPolicyOptions
    {
        public List<string> Polices { get; set; } = new List<string>();
        
    }
}
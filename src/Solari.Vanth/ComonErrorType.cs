﻿namespace Solari.Vanth
{
    public static class CommonErrorType
    {
        public const string ValidationError = "Validation-Error";
        public const string Exception = "Exception";
        public const string ApplicationError = "Application-Error";
        public const string IntegrationError = "Integration-Error";
    }
}
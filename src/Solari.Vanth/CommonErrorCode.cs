namespace Solari.Vanth
{
    public class CommonErrorCode
    {
        public const string ValidationErrorCode = "1";
        public const string ExceptionErrorCode = "500";
        public const string ApplicationErrorCode = "2";
        public const string IntegrationErrorCode = "3";
    }
}
﻿namespace Solari.Callisto.Abstractions
{
    public enum CallistoOperation
    {
        Insert,
        Update,
        Delete,
        Replace,
        Query,
        Aggregation,
        Pipeline,
        None
    }
}